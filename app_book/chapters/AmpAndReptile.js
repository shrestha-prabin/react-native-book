import React, { Component } from "react";
import GridDisplay from '../components/GridDisplay';

const data = [
    { id: '1', title: 'Lizard', imageUri: require('./imgs/animals/amphibian_reptile/Lizard.jpg') },
    { id: '2', title: 'Salamander', imageUri: require('./imgs/animals/amphibian_reptile/salamander.jpg') },
    { id: '3', title: 'Snake', imageUri: require('./imgs/animals/amphibian_reptile/Snake.jpg') },
    { id: '4', title: 'Toad', imageUri: require('./imgs/animals/amphibian_reptile/toad.jpg') },
    { id: '5', title: 'Tortoise', imageUri: require('./imgs/animals/amphibian_reptile/tortoise.jpg') },
    // { id: '6', title: '', imageUri: require('./imgs/animals/amphibian_reptile/.jpg') },
    // { id: '7', title: '', imageUri: require('./imgs/animals/amphibian_reptile/.jpg') },
    // { id: '8', title: '', imageUri: require('./imgs/animals/amphibian_reptile/.jpg') },
    // { id: '9', title: '', imageUri: require('./imgs/animals/amphibian_reptile/.jpg') },
    // { id: '10', title: '', imageUri: require('./imgs/animals/amphibian_reptile/.jpg') }
]

class AmpAndReptile extends Component {
    render() {
        return (
            <GridDisplay data={data} />
        )
    }
}

export default AmpAndReptile;