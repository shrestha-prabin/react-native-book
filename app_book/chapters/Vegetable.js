import React, { Component } from "react";
import { StyleSheet, Text } from 'react-native';
import GridDisplay from './../components/GridDisplay';

const data = [
    {id: 'cauliflower', title:'Cauliflower (फुलगोभी)', imageUri: require('./imgs/vegetables/cauliflower.png')},
    {id: 'ladysfinger', title:'Lady\'s Finger (भिन्डी)', imageUri: require('./imgs/vegetables/ladysfinger.png')},
    {id: 'carrot', title:'Carrot (गाजर)', imageUri: require('./imgs/vegetables/carrot.png')},
    {id: 'cabbage', title:'Cabbage (बन्दा)', imageUri: require('./imgs/vegetables/cabbage.png')},
    {id: 'spinach', title:'Spinach (पालुङ्गो)', imageUri: require('./imgs/vegetables/spinach.png')},
    {id: 'brinjal', title:'Brinjal (भान्टा)', imageUri: require('./imgs/vegetables/brinjal.png')},
    {id: 'garlic', title:'Garlic (लसुन)', imageUri: require('./imgs/vegetables/garlic.png')},
    {id: 'onion', title:'Onion (प्याज)', imageUri: require('./imgs/vegetables/onion.png')},
    {id: 'radish', title:'Radish (मूला)', imageUri: require('./imgs/vegetables/radish.png')},
    {id: 'capsicum', title:'Capsicum (भेडे खु्र्सानी)', imageUri: require('./imgs/vegetables/capsicum.png')} 
]
class Vegetable extends Component {
    render() {
        return (
            <GridDisplay data={data} />
        )
    }
}

export default Vegetable;