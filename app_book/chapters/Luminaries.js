import React, { Component } from "react";
import PPSizeGridDisplay from './../components/PPSizeGridDisplay';

const data = [
    { id: '1', title: 'Amar Singh Thapa', imageUri: require('./imgs/luminaries/amar_singh_thapa.jpg') },
    { id: '2', title: 'Anshuvarma', imageUri: require('./imgs/luminaries/anshuvarma.jpg') },
    { id: '3', title: 'Araniko', imageUri: require('./imgs/luminaries/araniko.jpg') },
    { id: '4', title: 'Balbhadra Kunwar', imageUri: require('./imgs/luminaries/balbhadra_kunwar.jpg') },
    { id: '5', title: 'Bhanubhakta Acharya', imageUri: require('./imgs/luminaries/bhanubhakta_acharya.jpg') },
    { id: '6', title: 'Bhimsen Thapa', imageUri: require('./imgs/luminaries/bhimsen_thapa.jpg') },
    { id: '7', title: 'Devi Sita', imageUri: require('./imgs/luminaries/devi_sita.jpg') },
    { id: '8', title: 'King Janak', imageUri: require('./imgs/luminaries/king_janak.jpg') },
    { id: '9', title: 'Trivbhuvan Bir Bikram Shah', imageUri: require('./imgs/luminaries/king_tribhuvan.jpg') },
    { id: '10', title: 'Gautam Buddha', imageUri: require('./imgs/luminaries/lord_buddha.jpg') },
    { id: '10', title: 'Motiram Bhatta', imageUri: require('./imgs/luminaries/motiram_bhatta.jpg') },
    { id: '10', title: 'Pasang Lhamu Sherpa', imageUri: require('./imgs/luminaries/pasang_lhamu_sherpa.jpg') },
    { id: '10', title: 'Prihvi Narayan Shah', imageUri: require('./imgs/luminaries/prithivi_narayan_shah.jpg') },
    { id: '10', title: 'Ram Shah', imageUri: require('./imgs/luminaries/ram_shah.jpg') },
    { id: '10', title: 'Shankhadhar Shakhwa', imageUri: require('./imgs/luminaries/shankhadhar.jpg') }
]

class Luminaries extends Component {
    render() {
        return (
            <PPSizeGridDisplay data={data} />
        )
    }
}

export default Luminaries;