import React, { Component } from "react";
import FullWidthGridDisplay from '../components/FullWidthGridDisplay';

const data = [
    { id: '1', title: 'Get up early in the morning', imageUri: require('./imgs/habit/get_up_early.jpg') },
    { id: '2', title: 'Brush your teeth twice a day', imageUri: require('./imgs/habit/brushing.jpg') },
    { id: '3', title: 'Take a bath everyday', imageUri: require('./imgs/habit/bathing.jpg') },
    { id: '4', title: 'Comb your hair neatly', imageUri: require('./imgs/habit/combing.jpg') },
    { id: '5', title: 'Wash your hands before meals', imageUri: require('./imgs/habit/wash_hands.jpg') },
    { id: '5', title: 'Eat fresh and healthy food', imageUri: require('./imgs/habit/hygienic_food.jpg') },
    { id: '6', title: 'Drink plenty of water', imageUri: require('./imgs/habit/drinking.jpg') },
    { id: '7', title: 'Finish your homework in time', imageUri: require('./imgs/habit/writing.jpg') },
    { id: '8', title: 'Always put the garbage in dustbin', imageUri: require('./imgs/habit/throwing.jpg') },
    { id: '9', title: 'Cover your mouth while sneezing and coughing', imageUri: require('./imgs/habit/cover_mouth.jpg') },
    { id: '10', title: 'Cut your nails regularly', imageUri: require('./imgs/habit/nail_cutting.jpg') },
    { id: '11', title: 'Go to bed on time', imageUri: require('./imgs/habit/go_bed.jpg') },
]

class Habit extends Component {
    render() {
        return (
            <FullWidthGridDisplay data={data} />
        )
    }
}

export default Habit;