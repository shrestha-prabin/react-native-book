import React, { Component } from 'react';
import NepaliAlphabetView from './../components/NepaliAlphabetView';

class NepaliConsonant extends Component {

    symbols = ['क', 'ख', 'ग', 'घ', 'ङ'];

    words = ['कमल', 'खरायो', 'गमला', 'घर', 'नङ'];

    images = [
        require('./imgs/lotus.png'), require('./imgs/rabbit.png'), require('./imgs/pot.png'),
        require('./imgs/house.png'), require('./imgs/nail.png')
    ];

    drawTemplate = [
        require('./imgs/draw/c1.png'), require('./imgs/draw/c2.png'), require('./imgs/draw/c3.png'),
        require('./imgs/draw/c4.png'), require('./imgs/draw/c5.png')
    ];

    render() {
        return (
            <NepaliAlphabetView symbols={this.symbols}
            words ={this.words}
            images = {this.images}
            drawTemplate = {this.drawTemplate}/>
        )
    }
}

export default NepaliConsonant;