import React, { Component } from 'react';
import { StyleSheet, View, Text, TouchableOpacity, ImageBackground, Image } from 'react-native';
import MultipleImageView from './../components/MultipleImageView';
import { SketchCanvas } from '@terrylinla/react-native-sketch-canvas';

class Number extends Component {

    englishNumbers = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10'];
    englishWords = ['One', 'Two', 'Three', 'Four', 'Five', 'Six', 'Seven', 'Eight', 'Nine', 'Ten'];
    nepaliNumbers = ['१', '२', '३', '४', '५', '६', '७', '८', '९', '१०'];
    nepaliWords = ['एक', 'दुर्इ', 'तिन', 'चार', 'पाँच', 'छ', 'सात', 'अाठ', 'नाैँ', 'दश'];

    images = [
        require('./../chapters/imgs/lotus.png'), require('./../chapters/imgs/lotus.png'),
        require('./../chapters/imgs/lotus.png'), require('./../chapters/imgs/lotus.png'),
        require('./../chapters/imgs/lotus.png'), require('./../chapters/imgs/lotus.png'),
        require('./../chapters/imgs/lotus.png'), require('./../chapters/imgs/lotus.png'),
        require('./../chapters/imgs/lotus.png'), require('./../chapters/imgs/lotus.png'),
    ];

    drawTemplate = [
        require('./imgs/draw/num_1.png'), require('./imgs/draw/num_2.png'),
        require('./imgs/draw/num_3.png'), require('./imgs/draw/num_4.png'),
        require('./imgs/draw/num_1.png'), require('./imgs/draw/num_1.png'),
        require('./imgs/draw/num_1.png'), require('./imgs/draw/num_1.png'),
        require('./imgs/draw/num_1.png'), require('./imgs/draw/num_1.png')
    ];

    constructor() {
        super();
        i = 0;
    }

    loadPrevious = () => {
        if (--i < 0) i = 0;
        this.setState({ i })
        this.clearCanvas();
    }

    loadNext = () => {
        if (++i >= this.englishNumbers.length) i = this.englishNumbers.length - 1;
        this.setState({ i })
        this.clearCanvas();
    }

    clearCanvas = () => {
        this._sketchCanvas.clear();
    }

    render() {
        return (
            <View style={this.styles.container}>
                <HeaderBlock style={this.styles.header}
                    engNum={this.englishNumbers[i]}
                    engWord={this.englishWords[i]}
                    nepNum={this.nepaliNumbers[i]}
                    nepWord={this.nepaliWords[i]} />

                <MultipleImageView style={this.styles.imagesContainer}
                    imageUri={this.images[i]}
                    count={i + 1} />

                <ImageBackground style={this.styles.canvasContainer}
                    imageStyle={{ resizeMode: 'contain' }}
                    source={this.drawTemplate[i]}>
                    <SketchCanvas
                        style={this.styles.canvas}
                        ref={ref => this._sketchCanvas = ref}
                        strokeColor={'red'}
                        strokeWidth={5} />
                </ImageBackground>

                <View style={this.styles.buttonsContainer}>
                    <TouchableOpacity style={this.styles.buttonLeft} onPress={this.loadPrevious} activeOpacity={0.8}>
                        <Image style={this.styles.buttonImage} source={require('./../assets/button_left.png')} />
                    </TouchableOpacity>
                    <TouchableOpacity style={this.styles.buttonClear} onPress={this.clearCanvas} activeOpacity={0.8}>
                        <Image style={this.styles.buttonImage} source={require('./../assets/eraser.png')} />
                    </TouchableOpacity>
                    <TouchableOpacity style={this.styles.buttonRight} onPress={this.loadNext} activeOpacity={0.8}>
                        <Image style={this.styles.buttonImage} source={require('./../assets/button_right.png')} />
                    </TouchableOpacity>
                </View>

            </View>
        )
    }

    styles = StyleSheet.create({
        container: {
            flex: 1,
        },
        header: {
        },
        imagesContainer: {
        },
        canvasContainer: {
            flex: .5,
            width: '100%',
            backgroundColor: '#fff'
        },
        canvas: {
            flex:1,
            backgroundColor: 'transparent'
        },
        buttonsContainer: {
            height: 48,
            flexDirection: 'row',
            width: '100%',
            alignItems: 'stretch',
            backgroundColor: '#fff'
        },
        buttonLeft: {
            flex: 1.5,
            marginRight: 2,
            backgroundColor: '#669FD1',
            borderTopRightRadius: 10
        },
        buttonRight: {
            flex: 1.5,
            marginLeft: 2,
            backgroundColor: '#669FD1',
            borderTopLeftRadius: 10
        },
        buttonClear: {
            flex: 1,
            width: 75,
            borderTopLeftRadius: 10,
            borderTopRightRadius: 10,
            backgroundColor: '#669FD1'
        },
        buttonImage: {
            flex: 1,
            margin: 10,
            resizeMode: 'contain',
            alignSelf: 'center'
        }
    })
}

export default Number;

class HeaderBlock extends Component {

    render() {
        return (
            <View style={this.styles.container}>
                <TouchableOpacity style={this.styles.engBlock}>
                    <Text style={this.styles.textEngNum}>{this.props.engNum}</Text>
                    <Text style={this.styles.textEngWord}>{this.props.engWord}</Text>
                </TouchableOpacity>

                <View style={{ height: '90%', width: StyleSheet.hairlineWidth, backgroundColor: '#abc' }} />
                <TouchableOpacity style={this.styles.nepBlock}>
                    <Text style={this.styles.textNepNum}>{this.props.nepNum}</Text>
                    <Text style={this.styles.textNepWord}>{this.props.nepWord}</Text>
                </TouchableOpacity>

            </View>
        )
    }

    styles = StyleSheet.create({
        container: {
            flexDirection: 'row',
            alignItems: 'center',
            backgroundColor: '#fff'
        },

        engBlock: {
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center'
        },
        nepBlock: {
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center'
        },
        textEngNum: {
            fontSize: 60,
            color: '#119119'
        },
        textEngWord: {
            fontSize: 30,
            color: '#119119'
        },
        textNepNum: {
            fontSize: 60,
            color: '#c0392b'
        },
        textNepWord: {
            fontSize: 30,
            color: '#c0392b'
        },
    })
}