import React, { Component } from "react";
import GridDisplay from './../components/GridDisplay';

const data = [
    { id: '1', title: 'Bowl', imageUri: require('./imgs/kitchen/bowl.jpg') },
    { id: '2', title: 'Cup', imageUri: require('./imgs/kitchen/cup.jpg') },
    { id: '3', title: 'Fork', imageUri: require('./imgs/kitchen/fork.jpg') },
    { id: '4', title: 'Gas Stove', imageUri: require('./imgs/kitchen/gas_stove.jpg') },
    { id: '5', title: 'Glass', imageUri: require('./imgs/kitchen/glass.jpg') },
    { id: '6', title: 'Grater', imageUri: require('./imgs/kitchen/grater.jpg') },
    { id: '7', title: 'Grinder', imageUri: require('./imgs/kitchen/grinder.jpg') },
    { id: '8', title: 'Hot Case', imageUri: require('./imgs/kitchen/hot_case.jpg') },
    { id: '9', title: 'Jug', imageUri: require('./imgs/kitchen/jug.jpg') },
    { id: '10', title: 'Kettle', imageUri: require('./imgs/kitchen/kettle.jpg') },
    { id: '11', title: 'Knife', imageUri: require('./imgs/kitchen/knife.jpg') },
    { id: '12', title: 'Ladle', imageUri: require('./imgs/kitchen/ladle.jpg') },
    { id: '13', title: 'Plate', imageUri: require('./imgs/kitchen/plate.jpg') },
    { id: '14', title: 'Pressure Cooker', imageUri: require('./imgs/kitchen/pressure_cooker.jpg') },
    { id: '15', title: 'Rice Cooker', imageUri: require('./imgs/kitchen/rice_cooker.jpg') },
    { id: '16', title: 'Seive', imageUri: require('./imgs/kitchen/seive.jpg') },
    { id: '17', title: 'Spatula', imageUri: require('./imgs/kitchen/spatula.jpg') },
    { id: '18', title: 'Spoon', imageUri: require('./imgs/kitchen/spoon.jpg') },
    { id: '19', title: 'Squeezer', imageUri: require('./imgs/kitchen/squeezer.jpg') },
    { id: '20', title: 'Thermos', imageUri: require('./imgs/kitchen/thermos.jpg') },
    { id: '21', title: 'Water Filter', imageUri: require('./imgs/kitchen/water_filter.jpg') }
]

class Kitchen extends Component {
    render() {
        return (
            <GridDisplay data={data} />
        )
    }
}

export default Kitchen;