import React, { Component } from 'react';
import NepaliAlphabetView from './../components/NepaliAlphabetView';

class NepaliVowel extends Component {

    symbols = ['अ', 'अा', 'इ'];

    words = ['अनार', 'अामा', 'इन्द्रेनी'];

    images = [
        require('./imgs/pomegranate.png'), require('./imgs/aama.png'), require('./imgs/rainbow.png')
    ];

    drawTemplate = [
        require('./imgs/draw/v1.png'), require('./imgs/draw/v2.png'), require('./imgs/draw/v3.png')
    ];

    render() {
        return (
            <NepaliAlphabetView symbols={this.symbols}
                words={this.words}
                images={this.images}
                drawTemplate={this.drawTemplate} />)
    }
}

export default NepaliVowel;