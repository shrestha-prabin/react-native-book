import React, { Component } from "react";
import FullWidthGridDisplay from '../components/FullWidthGridDisplay';

const data = [
    { id: '5', title: 'Basketball', imageUri: require('./imgs/game/basketball.jpg') },
    { id: '6', title: 'Boxing', imageUri: require('./imgs/game/boxing.jpg') },
    { id: '7', title: 'Carromboard', imageUri: require('./imgs/game/carrom_board.jpg') },
    { id: '1', title: 'Cricket', imageUri: require('./imgs/game/cricket.jpg') },
    { id: '8', title: 'Football', imageUri: require('./imgs/game/football.jpg') },
    { id: '2', title: 'Hokey', imageUri: require('./imgs/game/hokey.jpg') },
    { id: '3', title: 'Karate', imageUri: require('./imgs/game/karate.jpg') },
    { id: '4', title: 'Polo', imageUri: require('./imgs/game/polo.jpg') },
    { id: '9', title: 'Running', imageUri: require('./imgs/game/running.jpg') },
    { id: '10', title: 'Taekwondo', imageUri: require('./imgs/game/taekwondo.jpg') },
    { id: '10', title: 'Tennis', imageUri: require('./imgs/game/tennis.jpg') },
    { id: '10', title: 'Vollyball', imageUri: require('./imgs/game/vollyball.jpg') }
]

class Game extends Component {
    render() {
        return (
            <FullWidthGridDisplay data={data} />
        )
    }
}

export default Game;