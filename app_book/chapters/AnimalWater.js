import React, { Component } from "react";
import GridDisplay from '../components/GridDisplay';

const data = [
    { id: '', title: 'Anglefish', imageUri: require('./imgs/animals/water_animal/angelfish.jpg') },
    { id: '', title: 'Crab', imageUri: require('./imgs/animals/water_animal/crab.jpg') },
    { id: '', title: 'Dolphin', imageUri: require('./imgs/animals/water_animal/dolphin.jpg') },
    { id: '', title: 'Eel', imageUri: require('./imgs/animals/water_animal/eel.jpg') },
    { id: '', title: 'Fish', imageUri: require('./imgs/animals/water_animal/fish.jpg') },
    { id: '', title: 'Lobster', imageUri: require('./imgs/animals/water_animal/lobster.jpg') },
    { id: '', title: 'Octopus', imageUri: require('./imgs/animals/water_animal/octopus.jpg') },
    { id: '', title: 'Seal', imageUri: require('./imgs/animals/water_animal/seal.jpg') },
    { id: '', title: 'Sea Lion', imageUri: require('./imgs/animals/water_animal/sealion.jpg') },
    { id: '', title: 'Shark', imageUri: require('./imgs/animals/water_animal/shark.jpg') },
    { id: '', title: 'Starfish', imageUri: require('./imgs/animals/water_animal/starfish.jpg') },
    { id: '', title: 'Turtle', imageUri: require('./imgs/animals/water_animal/turtle.png') },
    { id: '', title: 'Walrus', imageUri: require('./imgs/animals/water_animal/walrus.jpg') },
    { id: '', title: 'Whale', imageUri: require('./imgs/animals/water_animal/whale.jpg') },
]

class AnimalWater extends Component {
    render() {
        return (
            <GridDisplay data={data} />
        )
    }
}

export default AnimalWater;