import React, { Component } from "react";
import FullWidthGridDisplay from '../components/FullWidthGridDisplay';

const data = [
    { id: '1', title: 'Bathing', imageUri: require('./imgs/our_action/Bathing.jpg') },
    { id: '2', title: 'Carrying', imageUri: require('./imgs/our_action/Carrying.jpg') },
    { id: '3', title: 'Crawling', imageUri: require('./imgs/our_action/crawling.jpg') },
    { id: '17', title: 'Cleaning', imageUri: require('./imgs/our_action/cleaning.jpg') },
    { id: '4', title: 'Crying', imageUri: require('./imgs/our_action/Crying.jpg') },
    { id: '5', title: 'Dancing', imageUri: require('./imgs/our_action/Dancing.jpg') },
    { id: '6', title: 'Drinking', imageUri: require('./imgs/our_action/drinking.jpg') },
    { id: '7', title: 'Eating', imageUri: require('./imgs/our_action/Eating.jpg') },
    { id: '8', title: 'Jumping', imageUri: require('./imgs/our_action/Jumping.jpg') },
    { id: '9', title: 'Nail Cutting', imageUri: require('./imgs/our_action/Nail_cutting.jpg') },
    { id: '10', title: 'Painting', imageUri: require('./imgs/our_action/Painting.jpg') },
    { id: '11', title: 'Playing', imageUri: require('./imgs/our_action/Playing.jpg') },
    { id: '12', title: 'Pulling', imageUri: require('./imgs/our_action/Pulling.jpg') },
    { id: '13', title: 'Pushing', imageUri: require('./imgs/our_action/Pushing.jpg') },
    { id: '14', title: 'Reading', imageUri: require('./imgs/our_action/Reading.jpg') },
    { id: '15', title: 'Running', imageUri: require('./imgs/our_action/Running.jpg') },
    { id: '16', title: 'Singing', imageUri: require('./imgs/our_action/Singing.jpg') },
    { id: '18', title: 'Sleeping', imageUri: require('./imgs/our_action/Sleeping.jpg') },
    { id: '19', title: 'Walking', imageUri: require('./imgs/our_action/Walking.jpg') },
    { id: '20', title: 'Writing', imageUri: require('./imgs/our_action/Writing.jpg') },
]

class Action extends Component {
    render() {
        return (
            <FullWidthGridDisplay data={data} />
        )
    }
}

export default Action;