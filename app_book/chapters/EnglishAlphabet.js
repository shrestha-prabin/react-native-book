import React, { Component } from "react";
import { StyleSheet, View, Text, TouchableOpacity, Button, Image, ImageBackground } from 'react-native';
import { SketchCanvas } from '@terrylinla/react-native-sketch-canvas';

class EnglishAphabet extends Component {

    capitalSym = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
        'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
    smallSym = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
        'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];

    nepaliSym = ['ए', 'बी', 'सी', 'डी', 'र्इ', 'एफ्', 'जी', 'एच्', 'अार्इ', 'जे', 'के', 'एल्', 'एम्',
        'एन्', 'अाे', 'पी', 'क्यु', 'अार', 'एस्', 'टी', 'यू', 'भी', 'डब्ल्यु', 'एक्स', 'वार्इ', 'जेड्'];

    wordEnglish = ['Apple', 'Banana', 'Cow', 'Dog', 'Elephant', 'Frog', 'Guava', 'House', 'Ice Cream', 'Jeep',
        'Kangaroo', 'Lady\'s Finger', 'Mango', 'Nightingale', 'Onion', 'Potato', 'Quail', 'Rice', 'Strawberry', 'Tea',
        'Uniform', 'Vegetable', 'Watermelon', 'X-mas Tree', 'Yak', 'Zebra'];

    wordNepali = ['स्याउ', 'केरा', 'गार्इ', 'कुकुर', 'हात्ती', 'भ्यागुता', 'अम्बा', 'घर', 'अार्इस क्रीम', 'जिप',
        'कङ्गारु', 'भिन्डी', 'अाँप', 'जुरेली', 'प्याज', 'अालु', 'बट्टाइ', 'भात', 'स्ट्रबेरी', 'चिया',
        'पाेशाक', 'तरकारी', 'खरबुजा', 'क्रिसमस रूख', 'चाैंरी', 'जीब्रा'];

    images = [
        require('./imgs/alphabets/picture/apple.png'), require('./imgs/alphabets/picture/banana.png'), require('./imgs/alphabets/picture/Cow.jpg'),
        require('./imgs/alphabets/picture/dog.png'), require('./imgs/alphabets/picture/elephant.jpg'), require('./imgs/alphabets/picture/frog.jpg'),
        require('./imgs/alphabets/picture/guava.jpg'), require('./imgs/alphabets/picture/House.jpg'), require('./imgs/alphabets/picture/ice_cream.jpg'),
        require('./imgs/alphabets/picture/Jeep.jpg'), require('./imgs/alphabets/picture/kangaroo.jpg'), require('./imgs/alphabets/picture/LadysFinger.jpg'),
        require('./imgs/alphabets/picture/mango.jpg'), require('./imgs/alphabets/picture/Nightingale.jpg'), require('./imgs/alphabets/picture/onion.png'),
        require('./imgs/alphabets/picture/potato.jpg'), require('./imgs/alphabets/picture/Quail.jpg'), require('./imgs/alphabets/picture/Rice.jpg'),
        require('./imgs/alphabets/picture/Strawberry.jpg'), require('./imgs/alphabets/picture/Tea.jpg'), require('./imgs/alphabets/picture/uniform.jpg'),
        require('./imgs/alphabets/picture/Vegetables.jpg'), require('./imgs/alphabets/picture/Watermelon.jpg'), require('./imgs/alphabets/picture/xmas.jpg'),
        require('./imgs/alphabets/picture/Yak.jpg'), require('./imgs/alphabets/picture/zebra.jpg'),

    ];

    drawTemplate = [
        require('./imgs/alphabets/a.png'), require('./imgs/alphabets/b.png'), require('./imgs/alphabets/c.png'),
        require('./imgs/alphabets/d.png'), require('./imgs/alphabets/e.png'), require('./imgs/alphabets/f.png'),
        require('./imgs/alphabets/g.png'), require('./imgs/alphabets/h.png'), require('./imgs/alphabets/i.png'),
        require('./imgs/alphabets/j.png'), require('./imgs/alphabets/k.png'), require('./imgs/alphabets/l.png'),
        require('./imgs/alphabets/m.png'), require('./imgs/alphabets/n.png'), require('./imgs/alphabets/o.png'),
        require('./imgs/alphabets/p.png'), require('./imgs/alphabets/q.png'), require('./imgs/alphabets/r.png'),
        require('./imgs/alphabets/s.png'), require('./imgs/alphabets/t.png'), require('./imgs/alphabets/u.png'),
        require('./imgs/alphabets/v.png'), require('./imgs/alphabets/w.png'), require('./imgs/alphabets/x.png'),
        require('./imgs/alphabets/y.png'), require('./imgs/alphabets/z.png')
    ]

    constructor() {
        super();
        i = 0;
    }

    loadPrevious = () => {
        if (--i < 0) i = 0;
        this.setState({ i })
        this.clearCanvas();
    }

    loadNext = () => {
        if (++i >= this.capitalSym.length) i = this.capitalSym.length - 1;
        this.setState({ i })
        this.clearCanvas();
    }

    clearCanvas = () => {
        this._sketchCanvas.clear();
    }

    render() {
        return (
            <View style={this.styles.container}>
                <HeaderBlock capital={this.capitalSym[i]}
                    small={this.smallSym[i]}
                    nepali={this.nepaliSym[i]}
                    image={this.images[i]}
                    wordEnglish={this.wordEnglish[i]}
                    wordNepali={this.wordNepali[i]} />

                <ImageBackground style={this.styles.canvas} imageStyle={{ resizeMode: 'contain' }}
                    source={this.drawTemplate[i]}>
                    <SketchCanvas
                        ref={ref => this._sketchCanvas = ref}
                        style={this.styles.canvas}
                        strokeColor={'red'}
                        strokeWidth={5} />

                </ImageBackground>

                <View style={this.styles.buttonsContainer}>
                    {/* <Button title='←' style={this.styles.buttonLeft} onPress={this.loadPrevious} /> */}
                    {/* <Button title='→' style={this.styles.buttonRight} onPress={this.loadNext} /> */}

                    <TouchableOpacity style={this.styles.buttonLeft} onPress={this.loadPrevious} activeOpacity={0.8}>
                        <Image style={this.styles.buttonImage} source={require('./../assets/button_left.png')} />
                    </TouchableOpacity>
                    <TouchableOpacity style={this.styles.buttonClear} onPress={this.clearCanvas} activeOpacity={0.8}>
                        <Image style={this.styles.buttonImage} source={require('./../assets/eraser.png')} />
                    </TouchableOpacity>
                    <TouchableOpacity style={this.styles.buttonRight} onPress={this.loadNext} activeOpacity={0.8}>
                        <Image style={this.styles.buttonImage} source={require('./../assets/button_right.png')} />
                    </TouchableOpacity>
                </View>
            </View>
        )
    }

    styles = StyleSheet.create({
        container: {
            flex: 1,
            backgroundColor: '#fff'
        },
        header: {
        },
        canvas: {
            flexGrow: 1,
            width: '100%',
            backgroundColor: 'transparent'
        },
        buttonsContainer: {
            flexDirection: 'row',
            height: 48,
            width: '100%',
            alignItems: 'stretch',
        },
        buttonLeft: {
            flex: 1.5,
            marginRight: 2,
            backgroundColor: '#669FD1',
            borderTopRightRadius: 10
        },
        buttonRight: {
            flex: 1.5,
            marginLeft: 2,
            backgroundColor: '#669FD1',
            borderTopLeftRadius: 10
        },
        buttonClear: {
            flex: 1,
            width: 75,
            borderTopLeftRadius: 10,
            borderTopRightRadius: 10,
            backgroundColor: '#669FD1'
        },
        buttonImage: {
            flex: 1,
            margin: 10,
            resizeMode: 'contain',
            alignSelf: 'center'
        }
    })
}

export default EnglishAphabet;

class HeaderBlock extends Component {

    render() {
        return (

            <View style={this.styles.container}>
                <TouchableOpacity style={this.styles.characterConainer} activeOpacity={0.9}>
                    <View style={this.styles.horizontal}>
                        <Text style={this.styles.textCapital}>{this.props.capital}</Text>
                        <Text style={this.styles.textSmall}>{this.props.small}</Text>
                    </View>

                    <Text style={this.styles.textNepali}>{this.props.nepali}</Text>
                </TouchableOpacity>

                <TouchableOpacity style={this.styles.imageContainer} activeOpacity={0.9}>
                    <Image style={this.styles.image} source={this.props.image} />

                    <View style={this.styles.horizontal}>
                        {/* <Text style={this.styles.wordEnglish}>{this.props.wordEnglish}</Text> */}
                        <RedText text={this.props.wordEnglish} />
                        <Text style={this.styles.wordNepali}>{"  " + this.props.wordNepali}</Text>
                    </View>
                </TouchableOpacity>
            </View>
        )
    }

    styles = StyleSheet.create({
        container: {
            flexDirection: 'row',
            justifyContent: 'space-between',
            backgroundColor: '#fff',
            padding: 12,
            elevation: 5
        },
        characterConainer: {
            height: 150,
            width: 150,
            elevation: 2,
            backgroundColor: '#c0392b',
            justifyContent: 'center',
            alignItems: 'center',
            borderRadius: 10,
            elevation: 5
        },
        horizontal: {
            flexDirection: 'row'
        },
        textCapital: {
            fontSize: 70,
            color: '#fff'
        },
        textSmall: {
            fontSize: 70,
            color: '#fff'
        },
        textNepali: {
            fontSize: 35,
            color: '#fff'
        },
        imageContainer: {
            height: 160,
            flexGrow: 1,
            backgroundColor: '#fff',
            justifyContent: 'center',
            alignItems: 'center'
        },
        image: {
            width: '90%',
            height: '80%',
            resizeMode: 'contain'
        },
        wordEnglish: {
            fontSize: 20,
        },
        wordNepali: {
            fontSize: 18,
            color: '#cf000f'
        }
    })
}

class RedText extends Component {

    render() {
        text = this.props.text;

        firstChar = text.charAt(0);
        remChar = text.substring(1, text.length);

        return (
            <View style={{ flexDirection: 'row' }}>
                <Text style={{ color: '#cf000f', fontSize: 20 }}>{firstChar}</Text>
                <Text style={{ fontSize: 20 }}>{remChar}</Text>
            </View>
        )
    }
}