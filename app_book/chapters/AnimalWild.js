import React, { Component } from "react";
import { StyleSheet, Text } from 'react-native';
import GridDisplay from './../components/GridDisplay';

const data = [
    { id: '1', title: 'Bat', imageUri: require('./imgs/animals/wild/bat.png') },
    { id: '2', title: 'Bear', imageUri: require('./imgs/animals/wild/bear.png') },
    { id: '3', title: 'Deer', imageUri: require('./imgs/animals/wild/deer.png') },
    { id: '4', title: 'Elephant', imageUri: require('./imgs/animals/wild/elephant.png') },
    { id: '5', title: 'Hippo', imageUri: require('./imgs/animals/wild/hippo.jpg') },
    { id: '6', title: 'Kangaroo', imageUri: require('./imgs/animals/wild/kangaroo.jpg') },
    { id: '7', title: 'Leopard', imageUri: require('./imgs/animals/wild/leopard.png') },
    { id: '8', title: 'Lion', imageUri: require('./imgs/animals/wild/lion.png') },
    { id: '9', title: 'Monkey', imageUri: require('./imgs/animals/wild/monkey.png') },
    { id: '10', title: 'Rabbit', imageUri: require('./imgs/animals/wild/rabbit.jpg') },
    { id: '11', title: 'Rhinoceros', imageUri: require('./imgs/animals/wild/rhinoceros.png') },
    { id: '12', title: 'Tiger', imageUri: require('./imgs/animals/wild/tiger.png') },
    { id: '13', title: 'Wolf', imageUri: require('./imgs/animals/wild/wolf.jpg') },
    { id: '14', title: 'Zebra', imageUri: require('./imgs/animals/wild/zebra.png') },

]
class AnimalWild extends Component {
    render() {
        return (
            <GridDisplay data={data} />
        )
    }
}

export default AnimalWild;