import React, { Component } from 'react';
import { StyleSheet, TouchableOpacity, Image, Text, View } from 'react-native';
import GridView from 'react-native-gridview';
import Lightbox from 'react-native-lightbox';

class FullWidthGridDisplay extends Component {

    render() {
        const data = this.props.data;

        itemsPerRow = 1;
        const randomData = [];
        for (let i = 0; i < data.length; i) {
            const endIndex = Math.max(Math.round(Math.random() * itemsPerRow), 0) + i;
            randomData.push(data.slice(i, endIndex));
            i = endIndex;
        }

        const dataSource = new GridView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2,
        }).cloneWithRows(randomData);

        return (
            <GridView
                data={data}
                dataSource={false ? dataSource : null}
                itemsPerRow={itemsPerRow}
                itemsPerRowLandscape={2}
                renderItem={(item) => {
                    return (
                        <GridItem gridItem={item} />
                    );
                }}
            />
        )
    }
}

export default FullWidthGridDisplay;

class GridItem extends Component {

    render({ gridItem } = this.props) {
        const { id, title, imageUri } = gridItem;
        return (
            <Lightbox
                springConfig={{ tension: 300, friction: 40 }}
                swipeToDismiss={false}
                backgroundColor={'#fff'}
                underlayColor={'#fff'}

                renderHeader={close => (
                    <TouchableOpacity onPress={close}>
                        <Image source={require('./imgs/button_close.png')} style={styles.closeButton} />
                    </TouchableOpacity>
                )}

                renderContent={() =>
                    <View style={styles.fullScreenContainer}>
                        <Image source={imageUri} style={styles.imageFull} />
                        <Text style={styles.textFull}>{title}</Text>
                    </View>
                }>

                <View style={styles.container}>
                    <Image source={imageUri} style={styles.image} />
                    <Text style={styles.text}>{title}</Text>
                </View>
            </Lightbox>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        margin: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
        elevation: 1
    },
    image: {
        height: 200,
        width: '100%',
        resizeMode: 'cover'
    },
    text: {
        fontSize: 18,
        justifyContent: 'center',
        padding: 4,
        color: '#6f7878'
    },
    fullScreenContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    imageFull: {
        flex: 0.8,
        width: '100%',
        resizeMode: 'contain',
        marginTop: 20
    },
    textFull: {
        flex: 0.1,
        fontSize: 18,
        color: '#6f7878',
        justifyContent: 'center',
        textAlign: 'center',
        position: 'absolute',
        bottom: 10,
        left: 0,
        width: '100%'
    },
    closeButton: {
        height: 30,
        width: 30,
        marginTop: 12,
        marginLeft: 12,
        resizeMode: 'contain'
    }
});
