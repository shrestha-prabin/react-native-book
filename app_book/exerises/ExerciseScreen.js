import React, { Component } from 'react';
import { StyleSheet, Text, View, FlatList, Alert, View, TouchableNativeFeedback } from 'react-native';

import { Actions } from 'react-native-router-flux';

class ExerciseScreen extends Component {

    render() {
        return (
            <View>
                <Text>Select one of the following exercises</Text>

                <Button
                    title='Match Pictures'
                    backgroundColor='#0a7a99'
                    onPress={() => Actions.match_the_following({ data: this.props.matchPictureData })} />

                <Button title='Fill Blanks' backgroundColor='#0a997a' />

            </View>
        )
    }
}

export default ExerciseScreen;

class Button extends Component {
    render() {
        return (
            <TouchableNativeFeedback
                style={{ width: '100%' }}
                background={TouchableNativeFeedback.SelectableBackground()}
                onPress={this.props.onPress}>

                <View
                    style={{
                        height: 50,
                        width: '100%',
                        backgroundColor: this.props.backgroundColor
                    }} >
                    <Text
                        style={{
                            color: '#fff',
                            fontWeight: 'bold',
                            fontSize: 16,
                            textAlign: 'center'
                        }}>
                        {this.props.title}
                    </Text>
                </View>
            </TouchableNativeFeedback>
        )
    }
}