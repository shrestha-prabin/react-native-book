import React, { Component } from 'react';
import { StyleSheet, Text, View, BackHandler, Alert } from 'react-native';
import { Router, Scene, Actions } from 'react-native-router-flux';
import SplashScreen from 'react-native-splash-screen';

import { YellowBox } from 'react-native';
YellowBox.ignoreWarnings(['Warning: isMounted(...) is deprecated', 'Module RCTImageLoader']);

import AmpAndReptile from './app/chapters/AmpAndReptile';
import Bird from './app/chapters/Bird';
import Computer from './app/chapters/Computer';
import Flower from './app/chapters/Flower';
import Game from './app/chapters/Game';
import Habit from './app/chapters/Habit';
import Insect from './app/chapters/Insect';
import Job from './app/chapters/Job';
import Kitchen from './app/chapters/Kitchen';
import Luminaries from './app/chapters/Luminaries';
import OurAction from './app/chapters/OurAction';
import Stationery from './app/chapters/Stationery';
import Transportation from './app/chapters/Transportation';
import AnimalWater from './app/chapters/AnimalWater';

import HomeScreen from './app/home/HomeScreen'
import EnglishAlphabet from './app/chapters/EnglishAlphabet'
import NepaliConsonant from './app/chapters/NepaliConsonant';
import Number from './app/chapters/Number';
import Fruit from './app/chapters/Fruit';
import Vegetable from './app/chapters/Vegetable';
import AnimalDomestic from './app/chapters/AnimalDomestic';
import AnimalWild from './app/chapters/AnimalWild';
import NepaliVowel from './app/chapters/NepaliVowel';
import MatchTheFollowing from './app/exerises/MatchTheFollowing';

class App extends Component {

  componentDidMount() {
    SplashScreen.hide();
    BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
  }
  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
  }

  handleBackPress = () => {
    if (Actions.currentScene === 'home') {
      Alert.alert('Exit', 'Are you sure you want to exit?',
        [
          { text: 'Cancel' },
          { text: 'Yes', onPress: () => BackHandler.exitApp() }
        ],
      )
    }
  }

  render() {
    return (
      <Router>
        <Scene key='root'>
          <Scene key='home' component={HomeScreen} title='Home' initial />
          <Scene key='english_alphabet' component={EnglishAlphabet} title='English Alphabets' />
          <Scene key='nep_vowel' component={NepaliVowel} title={'नेपाली वर्णमाला: स्वर वर्ण'} />
          <Scene key='nep_consonant' component={NepaliConsonant} title='नेपाली वर्णमाला: व्यञ्जन वर्ण' />
          <Scene key='number' component={Number} title='Numerical Digit/अङ्क' />
          <Scene key='fruit' component={Fruit} title='Fruits' />
          <Scene key='vegetable' component={Vegetable} title='Vegetables' />
          <Scene key='domestic_animal' component={AnimalDomestic} title='Domestic Animals' />
          <Scene key='wild_animal' component={AnimalWild} title='Wild Animals' />
          <Scene key='flower' component={Flower} title='Flowers' />
          <Scene key='bird' component={Bird} title='Wild Animals' />
          <Scene key='water_animal' component={AnimalWater} title='Water Animals' />
          <Scene key='insect' component={Insect} title='Insects' />
          <Scene key='amp_rep' component={AmpAndReptile} title='Amphibians and Reptiles' />
          <Scene key='computer' component={Computer} title='Computers and Camera' />
          <Scene key='kitchen' component={Kitchen} title='Kitchen Utensils' />
          <Scene key='luminaries' component={Luminaries} title='National Lumimaries' />
          <Scene key='transportation' component={Transportation} title='Means of Transportation' />
          <Scene key='job' component={Job} title='Jobs and Occupations' />
          <Scene key='habit' component={Habit} title='Good Habits' />
          <Scene key='game' component={Game} title='Games' />
          <Scene key='our_action' component={OurAction} title='Our Actions' />
          <Scene key='stationery' component={Stationery} title='Stationery Items' />

          <Scene key='match_the_following' component={MatchTheFollowing} title='Match Pictures'/>
        </Scene>
      </Router>
    )
  }
}

export default App;